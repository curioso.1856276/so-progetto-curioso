# SO progetto curioso

## Progetto File System

Il progetto consiste in un file system basato su un file binario che si occupa della gestione di file di testo e cartelle. E' composto principalmente da 3 parti  che permettono di lavorare sul sistema eseguendo tutte le funzioni base di un file (creazione di file e cartelle, scrittura e lettura di file, cancellazione di file e cartelle).

I file di cartella sono file contenenti altri file e sono formati da un primo blocco (FirstDirectoryBlock) che contiene l'header, il FileControlBlock  e i blocchi iniziali dei file all'interno della cartella; i blocchi successivi sono formati da un header e un array di primi blocchi. 
Ciò vuol dire che un file di cartella è caratterizzato da un FirstControlBlock e da eventuali blocchi (DirectoryBlock).

I file di testo invece sono blocchi di dati contenenti informazioni casuali. Formati da un primo blocco (FirstFileBlock) contenente header, FileControlBlock e array con informazioni; i blocchi successivi sono formati da header e array.
Ciò vuol dire che un file di testo è caratterizzato da un FirstFileBlock e da eventuali blocchi (FileBlock).

### Struttura 
Le 3 parti principali sono:
- la bitmap
- il diskDriver
- il simpleFS

la BitMap gestisce i blocchi che potranno essere utilizzati per memorizzare i file e le cartelle.

Il DiskDriver crea il disco su cui saranno memorizzate tutte le informazioni del file system. Inoltre, fa da intermediario tra la BitMap e il simpleFS, permettendo di gestire l'effettiva scrittura e lettura dei dati.

Infine il simpleFS gestisce l'interazione tra l'utente e il file system, tramite le funzioni del DiskDriver, e la gestione del modo in cui le cartelle e i file devono essere organizzati nel disco e collegati tra loro.

### Esecuzione
L'esecuzione del programma consente di verificare che tutte le funzioni implementate funzioni correttamente. 
Nel main vi è una suddivisione, tramite un test, delle funzioni implementate così da poterle verificare separatamente. Durante l'esecuzione del programma verranno appunto richieste informazioni riguardo quale funzioni della struttura testare.

La compilazione può essere facilmente eseguita dal comando "make" e successivamente tramite "./simplefs_test" per far partire il programma. 
