#include "simplefs.c"
#include "bitmap.c"
#include "disk_driver.c"
#include "simplefs.h"
#include "bitmap.h"
#include "disk_driver.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define FALSE 0

//test su 1=bitmap, 2=diskDriver e 3=simpleFS

int test;
int use_global_test = FALSE;

int main(int agc, char** argv) {
  printf("FirstBlock size %ld\n", sizeof(FirstFileBlock));
  printf("DataBlock size %ld\n", sizeof(FileBlock));
  printf("FirstDirectoryBlock size %ld\n", sizeof(FirstDirectoryBlock));
  printf("DirectoryBlock size %ld\n\n", sizeof(DirectoryBlock));
  
  if(!test){
    printf("cosa vuoi testare?\n 1=BitMap\n 2=DiskDriver\n 3=SimpleFs\n\n");
    scanf("%d",&test);
  }
  if(test==1){ //scelta di bitmap
    printf("\n\n\nBITMAP\n\n");
    //devo testare la index,set e get
    printf("\n\n\n\n*** BITMAP ***\n\n");
	printf("Creo una bitmap con 3 entries: 69, 128, 1");
	BitMap* bmp=(BitMap*)malloc(sizeof(BitMap));
	bmp->num_bits = 24;
	bmp->entries = (char*)malloc(sizeof(char)*3);
	bmp->entries[0] = 69;
	bmp->entries[1] = 128;
	bmp->entries[2] = 1;
	
	printf("\nEntries[0] = ");
	BitMap_print(bmp, 0);
	printf("\nEntries[1] = ");
	BitMap_print(bmp, 1);
	printf("\nEntries[2] = ");
	BitMap_print(bmp, 2);
	printf("\n\nPrendo il primo bit a 0, a partire dalla posizione 7\n");
	printf("Risultato = %d [CORRETTO = 7]\n", BitMap_get(bmp, 7, 0));

	printf("\nSetto questo bit a 1\n");
	BitMap_set(bmp, 7, 1);
	printf("Risultato = ");
	BitMap_print(bmp, 0);
	printf(" [CORRETTO = 11000101]\n");
	
	printf("\nPrendo il primo bit a 1, a partire dalla posizione 8\n");
	printf("Risultato = %d [Corretto = 15]\n", BitMap_get(bmp, 8, 1));	
	free(bmp->entries);
	free(bmp);
    
  }
  else if(test==2){ //scelta diskdriver
    printf("\n\n\nDISKDRIVER\n\n");
    //devo testare la init, write, read e free
	DiskDriver disk;

	printf("\n\n\n\n*** DISK DRIVER ***\n\n");
	printf("Creo un disco con 3 blocchi: test.txt");

	DiskDriver_init(&disk, "test.txt", 3);
	printf("\n%d blocchi liberi\n\n", disk.header->free_blocks);
	
	int i;
	char txt[BLOCK_SIZE - sizeof(BlockHeader)];

	FileBlock* fb1 = (FileBlock*)malloc(sizeof(FileBlock));
	for(i = 0; i < BLOCK_SIZE - sizeof(BlockHeader); i++)
		txt[i] = '0';
	strcpy(fb1->data, txt);
	
	FileBlock* fb2 = (FileBlock*)malloc(sizeof(FileBlock));
	for(i = 0; i < BLOCK_SIZE - sizeof(BlockHeader); i++)
		txt[i] = '1';
	strcpy(fb2->data, txt);
	
	FileBlock* fb3 = (FileBlock*)malloc(sizeof(FileBlock));
	for(i = 0; i < BLOCK_SIZE - sizeof(BlockHeader); i++)
		txt[i] = '2';
	strcpy(fb3->data, txt);
	
	// Scrittura su disco
	printf(" * Scrittura *\n\n");
	
	printf("Scrittura sul blocco 0\n");
	DiskDriver_writeBlock(&disk, fb1, 0);
	DiskDriver_flush(&disk);
	printf("Blocchi liberi: %d\nPrimo blocco libero: %d\n\n", disk.header->free_blocks, disk.header->first_free_block);
	
	printf("Scrittura sul blocco 1\n");
	DiskDriver_writeBlock(&disk, fb2, 1);
	DiskDriver_flush(&disk);
	printf("Blocchi liberi: %d\nPrimo blocco libero: %d\n\n", disk.header->free_blocks, disk.header->first_free_block);
	
	printf("Scrittura sul blocco 2\n");
	DiskDriver_writeBlock(&disk, fb3, 2);
	DiskDriver_flush(&disk);
	printf("Blocchi liberi: %d\nPrimo blocco libero: %d\n\n", disk.header->free_blocks, disk.header->first_free_block);
	
	// Leggo i blocchi che ho scritto
	printf(" * Lettura *\n\n");

	FileBlock* fb_read = (FileBlock*)malloc(sizeof(FileBlock));

	DiskDriver_readBlock(&disk, fb_read, 0);
	printf("Lettura del blocco 0: %s\n\n", fb_read->data);

	DiskDriver_readBlock(&disk, fb_read, 1);
	printf("Lettura del blocco 1: %s\n\n", fb_read->data);

	DiskDriver_readBlock(&disk, fb_read, 2);
	printf("Lettura del blocco 2: %s\n\n", fb_read->data);

	// Rilascio
	printf(" * Rilascio delle risorse *\n\n");

	DiskDriver_freeBlock(&disk, 0);
	DiskDriver_freeBlock(&disk, 1);
	DiskDriver_freeBlock(&disk, 2);
	printf("Blocchi liberi: %d\n\n", disk.header->free_blocks);
	
	free(fb1);
	free(fb2);
	free(fb3);
	free(fb_read);

	//Elimino il disco dal file system
	int ret = remove("test.txt");
	if(ret < 0){
		printf("ERRORE: remove()");
		return -1;
	}    
  }

  else{ //scelta simplefs
    printf("\n\n\nSIMPLEFS\n\n");
    //testare init, create,mk,read,open,write,change,seek,close,remove
	printf("Creo un nuovo disco di 128 blocchi e il FileSystem\n");
	printf("Spazio nel FDB: %ld\nSpazio nel DB: %ld", FDB, DB);
	DiskDriver disk2;
	SimpleFS sfs;
	DiskDriver_init(&disk2, "test2.txt", 128);
	DiskDriver_flush(&disk2);
	DirectoryHandle* dh = SimpleFS_init(&sfs, &disk2);
	if(dh == NULL){
		SimpleFS_format(&sfs);
		dh = SimpleFS_init(&sfs, &disk2);
		if(dh == NULL) exit(EXIT_FAILURE);
	}
	
	// Creo 130 file su un blocco da 128-header
	printf("\n\n * Creo 130 file * \n");
	char filename[10];
	int i;
	int ret;
	FileHandle* fh = NULL;
	for(i = 0; i < 110; i++){
		sprintf(filename, "%d", i);
		fh = SimpleFS_createFile(dh, filename);
	}
	if(fh != NULL) SimpleFS_close(fh);

	// Li leggo
	printf("\n * Leggo la directory corrente (root) * \n");
	char** files = (char**) malloc(sizeof(char*) * dh->dcb->num_entries);
	ret = SimpleFS_readDir(files, dh);
	for (i=0; i<dh->dcb->num_entries; i++)
		printf("%s ", files[i]);
	
	for (i=0; i<dh->dcb->num_entries; i++)
		free(files[i]);
	
	free(files);
	printf("\n\n");

	printf(" * Creo una directory d1 *\n\n");
	SimpleFS_mkDir(dh, "d1");
	if(dh == NULL) exit(EXIT_FAILURE);
	
	FirstDirectoryBlock* fdb = (FirstDirectoryBlock*)malloc(sizeof(FirstDirectoryBlock));
	
	ret = SimpleFS_readDir(files, dh);
	for (i = 0; i < dh->dcb->num_entries; i++)
		printf("%s ", files[i]);
	for (i = 0; i < dh->dcb->num_entries; i++)
		free(files[i]);

	printf("\n\n");

	printf(" * Creo di nuovo una directory d1 *\n");
	SimpleFS_mkDir(dh, "d1");
	if(dh == NULL) exit(EXIT_FAILURE);

	printf("\n * Entro in d1 *\n");
	SimpleFS_changeDir(dh, "d1");
	printf("Path: ");
	print_path(dh->sfs->disk, fdb, dh->dcb->fcb.block_in_disk);
	printf("\n");

	fh = NULL;
	for(i = 0; i < 7; i++){
		sprintf(filename, "%d", i);
		fh = SimpleFS_createFile(dh, filename);
	}
	if(fh != NULL) SimpleFS_close(fh);

	printf("\n * Esco da d1 *\n");
	SimpleFS_changeDir(dh, "..");
	printf("Path: ");
	print_path(dh->sfs->disk, fdb, dh->dcb->fcb.block_in_disk);
	free(fdb);

	printf("\n\n");

	printf("\n * Creo un file per fare i test read/write/seek * \n");
	FileHandle* fh_test = SimpleFS_createFile(dh, "file_test.txt");
	if(fh_test == NULL) exit(EXIT_FAILURE);
	else{
		FileHandle* f = SimpleFS_openFile(dh, "file_test.txt");
		printf("\n * Apertura del file di prova *\n");
		if(f == NULL) printf("ERRORE: il file non esiste\n");

		printf("\n * Scrittura del carattere '7' per 800 volte * '\n");
		// testo il wtite in FB
		char txt[800];
		for(i = 0; i < 800; i++){
			if(i%100 == 0) txt[i]='\n';
			else txt[i] = '7';
		}
		
		SimpleFS_write(f, txt, 800);

		// In realtà qui sovrascrivo quindi
		printf("\nCi sovrascrivo 'Questo è un file di prova, ora ne manipolo i dati'\n");

		SimpleFS_write(f, "Questo è un file di prova, ora ne manipolo i dati", 50);
		printf("\n\nLeggo il file di prova:\n");

		int size = 800;
		char* data = (char*)malloc(sizeof(char)*size+1);
		data[size] = '\0';
		SimpleFS_read(f, data, size);
		printf("%s", data);
		free(data);

		SimpleFS_seek(f, 0);

		printf("\n\n * Leggo i primi 26 bytes *:\n");

		size = 26;
		data = (char*)malloc(sizeof(char)*size+1);
		data[size] = '\0';
		SimpleFS_read(f, data, size);
		printf("%s\n\n\n", data);
		free(data);

		SimpleFS_close(fh_test);
	}

	printf(" * Rimuovo i primi tre file *\n");

	for(i = 0; i < 3; i++){
		sprintf(filename, "%d", i);
		SimpleFS_remove(dh, filename);
	}

	ret = SimpleFS_readDir(files, dh);
	for (i = 0; i < dh->dcb->num_entries; i++)
		printf("%s ", files[i]);
	for (i = 0; i < dh->dcb->num_entries; i++)
		free(files[i]);

	free(files);


	printf("\n\n");

	SimpleFS_format(&sfs);
  }

}
