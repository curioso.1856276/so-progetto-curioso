#include "simplefs.h"
#include "bitmap.h"
#include "disk_driver.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//semplifico i vari spazi 
#define FDB (BLOCK_SIZE-sizeof(BlockHeader)-sizeof(FileControlBlock)-sizeof(int))/sizeof(int)
#define FFB BLOCK_SIZE-sizeof(FileControlBlock) - sizeof(BlockHeader)
#define DB (BLOCK_SIZE-sizeof(BlockHeader))/sizeof(int)
#define FB BLOCK_SIZE-sizeof(BlockHeader)


// initializes a file system on an already made disk
// returns a handle to the top level directory stored in the first block
DirectoryHandle* SimpleFS_init(SimpleFS* fs, DiskDriver* disk){
    if(fs==NULL || disk==NULL){
        printf("Errore parametri");
        return NULL;
    }
    //assegno il disco al filesystem,creo directory e restituisco
    fs->disk=disk;
    FirstDirectoryBlock* fdb= malloc(sizeof(FirstDirectoryBlock));
    //controllo se esiste o no
    int ret=DiskDriver_readBlock(disk,fdb,0);
    if(ret==-1){
        free(fdb);
        return NULL;
    }
    //se non c'è creo handle e restituisco
    DirectoryHandle* dh=(DirectoryHandle*)malloc(sizeof(DirectoryHandle));
    dh->sfs=fs;
    dh->dcb=fdb;
    dh->directory=fdb; 
    dh->pos_in_block=0;

    return dh;
}

// creates the inital structures, the top level directory
// has name "/" and its control block is in the first position
// it also clears the bitmap of occupied blocks on the disk
// the current_directory_block is cached in the SimpleFS struct
// and set to the top level directory
void SimpleFS_format(SimpleFS* fs){
    if(fs==NULL)
        return;
    //memset(punto per riempire blocco di memoria, valore da impostare, num di byte da impostare a quel valore)
    memset(fs->disk->bitmap_data, 0, fs->disk->header->bitmap_entries);
    
    //assegnazione blocchi liberi
    fs->disk->header->free_blocks=fs->disk->header->num_blocks;
    fs->disk->header->first_free_block=0;
    /*
    //creo blocco 
    FirstDirectoryBlock* fb=malloc(sizeof(FirstDirectoryBlock));
    fb->header.previous_block=-1;
    fb->header.next_block=-1;
    fb->header.block_in_file=0;
    //inserisco le info
    fb->fcb.directory_block=-1;
    fb->fcb.block_in_disk=0;
    fb->fcb.is_dir=1;
    fb->fcb.size_in_blocks=1;*/
    //formatto la root
    FirstDirectoryBlock fdb={0};
    fdb.header.previous_block=-1;
    fdb.header.next_block=-1;
    fdb.header.block_in_file=0;
    //inserisco le info
    fdb.fcb.directory_block=-1;
    fdb.fcb.block_in_disk=0;
    fdb.fcb.is_dir=1;
    fdb.fcb.size_in_blocks=1;

    //copio la stringa puntata
    strcpy(fdb.fcb.name, "/");
    //memorizzo la fdb nel primo blocco 
    DiskDriver_writeBlock(fs->disk, &fdb,0); //fb,0);
    //return;
}

//funzione aggiunta per controllo file non esistente
int File_Dir(DiskDriver* disk, int space, int* file_blocks, const char* filename){
    FirstFileBlock ffbAux;
    for(int i=0; i<space; i++){
        if(file_blocks[i]>0 && (DiskDriver_readBlock(disk, &ffbAux, file_blocks[i]))!=-1){
            if(!strncmp(ffbAux.fcb.name, filename,128)) //per limitare il confronto fra le due stringhe al piu' 128 caratteri
                return i; 
        }
    }
    return -1;
}


// creates an empty file in the directory d
// returns null on error (file existing, no free blocks)
// an empty file consists only of a block of type FirstBlock

FileHandle* SimpleFS_createFile(DirectoryHandle* d, const char* filename){
    if(d==NULL || filename ==NULL)
        return NULL;
    /*
    //se file con stesso nome esiste ->errore
    if(SimpleFS_openFile(d, filename)!=NULL)
        return NULL;
    //se non ci sono blocchi liberi, errore
    if(d->sfs->disk->header->free_blocks < 1)
        return NULL;
    */
    FirstDirectoryBlock* fdb=d->dcb;
    DiskDriver* disk=d->sfs->disk;
    int ret;
    
    //controllo dove si trova il file, se in FDB o DB
    if(fdb->num_entries >0){
        //verifico se in FDB
        ret=File_Dir(disk, FDB, fdb->file_blocks, filename);
        if(ret>=0){ //file esiste gia in fdb
            printf("il file %s esiste già in FDB\n",filename);
            return NULL;
        }
        //se in DB
        DirectoryBlock db;
        int next_block=fdb->header.next_block;
        while(next_block!=-1){
            ret=DiskDriver_readBlock(disk, &db, next_block);
            if(ret<0)
                return NULL;
            ret=File_Dir(disk, DB, db.file_blocks, filename);
            if(ret>=0){ //file esiste già in db
                printf("il file %s esiste già in DB\n",filename);
                return NULL;
            }
            next_block=db.header.next_block;
        }
    }

    //dopo tutti controlli creo il file su blocco vuoto
    int block_free=DiskDriver_getFreeBlock(disk, disk->header->first_free_block);
    if(block_free==-1) //se blocchi non vuoti
        return NULL;
    
    FirstFileBlock* ffb_new=calloc(1, sizeof(FirstFileBlock)); //o malloc, verificare
    ffb_new->header.block_in_file=0;
    ffb_new->header.previous_block=-1;
    ffb_new->header.next_block=-1;
    ffb_new->fcb.directory_block=fdb->fcb.block_in_disk;
    ffb_new->fcb.block_in_disk=block_free;
    ffb_new->fcb.is_dir=0;
    ffb_new->fcb.size_in_bytes=0;
    ffb_new->fcb.size_in_blocks=1;
    strncpy(ffb_new->fcb.name, filename,128);

    //scrivo file su disco
    ret=DiskDriver_writeBlock(disk, ffb_new, block_free);
    if(ret<0)
        return NULL;
    
    //ricerco in fdb o db per poi salvare tutto
    int found=0;
    int db_create=0; //per segnalare se bisogno di creare nuova cartella
    int in_db=0; //0 se fdb, 1 se db
    int entry=0; 
    int block_dir=0; //blocco directory
    int block_disk=fdb->fcb.block_in_disk; //blocco disco
    int end_db=0; //per vedere se spazio esaurito o no
    int i=0;
    DirectoryBlock db;
    
    //cerco prima in fdb se posto libero
    if(fdb->num_entries<FDB){
        int* blocks=fdb->file_blocks;
        for(i=0;i<FDB;i++){
            if(blocks[i]==0){ //vi sono blocchi liberi in fdb
                found=1;
                entry=i;
                printf("trovati[%d] in fdb ", entry);
                break;
            }
        }
    }
    else{ //vado a cercare spazio nelle altre cartelle, in db
        in_db=1;
        int next_block=fdb->header.next_block;
        while(next_block!=-1 && !found){
            ret=DiskDriver_readBlock(disk, &db, next_block);
            if(ret==-1){
                printf("errore\n");
                DiskDriver_freeBlock(disk,fdb->fcb.block_in_disk);
                return NULL;
            }
            int* blocks=db.file_blocks;
            block_dir++;
            block_disk=next_block;
            for(i=0; i<DB; i++){
                if(blocks[i]==0){ //vi sono blocchi liberi in db
                    found=1;
                    entry=i;
                    printf("trovati[%d] in db ", entry);
                    break;
                }
                end_db++;
            }
            db_create=1; //non bisogno di creare cartella
            next_block=db.header.next_block;
        }
    }
    //controllo se ho esaurito spazio in db
    if(end_db>DB-1){
        printf("spazio esaurito in db, %d\n", end_db);
    }
    //se non c'è spazio neanche nelle altre cartelle ne creo nuove
    if(!found){
        printf("creazione db\n");
        DirectoryBlock new={0};
        new.header.next_block=-1;
        new.header.block_in_file=block_dir;
        new.header.previous_block=block_disk;
        new.file_blocks[0]=block_free;

        //prendo blocco libero per allocare
        int new_db=DiskDriver_getFreeBlock(disk, disk->header->first_free_block);
        if(new_db==-1){
            printf("nessun blocco libero\n");
            DiskDriver_freeBlock(disk,fdb->fcb.block_in_disk);
           return NULL;
        }
        //lo scrivo su disco
        ret=DiskDriver_writeBlock(disk,&new, new_db);
        if(ret == -1){
			printf("ERRORE: writeBlock()\n");
			DiskDriver_freeBlock(disk, fdb->fcb.block_in_disk);
			return NULL;
		}

        //se è 0 ho bisogno di creare nuova cartella
        if(db_create==0)
            fdb->header.next_block=new_db;
        else  
            db.header.next_block=new_db;
        
        db=new;
        block_disk=new_db;
    }
    //aggiorno le varie cartelle in fdb e db
    if(in_db==0){ //ci troviamo in fdb
        printf(" salvo %s in FDB\n",filename);
        fdb->num_entries++;
        fdb->file_blocks[entry]=block_free;
        DiskDriver_aggiorna(disk,fdb,fdb->fcb.block_in_disk);
    }
    else{ //in db
        printf(" salvo %s in DB\n",filename);
        fdb->num_entries++;
        DiskDriver_aggiorna(disk,fdb,fdb->fcb.block_in_disk);
        db.file_blocks[entry]=block_free;
        DiskDriver_aggiorna(disk,&db,block_disk);
    }

    //tutte info su disco e restituisco FileHandle
    FileHandle* fh=malloc(sizeof(FileHandle));
    fh->sfs=d->sfs;
    fh->fcb=ffb_new;
    fh->directory=fdb;
    fh->pos_in_file=0;
    return fh;
}

// reads in the (preallocated) blocks array, the name of all files in a directory 
int SimpleFS_readDir(char** names, DirectoryHandle* d){
    if(names==NULL || d==NULL)
        return -1;
    
    FirstDirectoryBlock* fdb=d->dcb;
    DiskDriver* disk=d->sfs->disk;
    int i, j=0, ret;
    
    if(fdb->num_entries >0){
        FirstFileBlock ffb;
        int* blocks=fdb->file_blocks;
        for(i=0; i<FDB; i++){
            if(blocks[i]<0)
                continue;
            ret=DiskDriver_readBlock(disk,&ffb,blocks[i]);
            if(blocks[i]>0 && ret!=-1){
                //copio il nome in names
                names[j]=strndup(ffb.fcb.name,128); //per richiedere argomento aggiuntivo per specificare num byte da copiare al max
                j++;
            }
        }
        //verifico anche nel DB nello stesso modo
        DirectoryBlock db;
        if(i<fdb->num_entries){
            int next_block = fdb->header.next_block;
            while(next_block!=-1){
                ret=DiskDriver_readBlock(disk, &db, next_block);
                if(ret!=0)
                    return -1;
                int* block=db.file_blocks;
                for(i=0; i< DB; i++){
                    ret=DiskDriver_readBlock(disk,&ffb,block[i]);
                    if(ret!=0)
                        return -1;
                    if(block[i]>0 && ret!=-1){
                        names[j]=strndup(ffb.fcb.name, 128);
                        j++;
                    }
                }
                next_block=db.header.next_block;
            }
        }
    }
    return 0;
}

void print_path(DiskDriver* disk, FirstDirectoryBlock* fdb, int block_in_disk){
    if(block_in_disk==-1)
        return;
    DiskDriver_readBlock(disk, fdb, block_in_disk);
    block_in_disk=fdb->fcb.directory_block;
    char dirname[128];
    strncpy(dirname,fdb->fcb.name,128);
    print_path(disk, fdb, block_in_disk);
    if(strcmp(dirname,"/")==0)
        printf("root");
    else   
        printf("/%s",dirname);
}

// opens a file in the  directory d. The file should be exisiting
FileHandle* SimpleFS_openFile(DirectoryHandle* d, const char* filename){
    if(d==NULL || filename==NULL)
        return NULL;
    FirstDirectoryBlock* fdb=d->dcb;
    DiskDriver* disk=d->sfs->disk;
    int ret;
    if(fdb->num_entries >0){ //se non è vuota
        FileHandle* fh=malloc(sizeof(FileHandle));
        fh->sfs=d->sfs;
        fh->directory=fdb;
        fh->pos_in_file=0;
        int cerca;
        FirstFileBlock* ffb_open=malloc(sizeof(FirstFileBlock));
        int pos=File_Dir(disk, FDB,fdb->file_blocks,filename);
        
        //cerco in FDB
        if(pos>=0){
            cerca=1;
            DiskDriver_readBlock(disk, ffb_open, fdb->file_blocks[pos]);
            fh->fcb=ffb_open;
        }
        //cerco in DB
        DirectoryBlock db;
        int next_block=fdb->header.next_block;
        while(next_block!=-1 && !cerca){
            ret=DiskDriver_readBlock(disk,&db, next_block);
            pos=File_Dir(disk, DB, db.file_blocks,filename);
            if(pos>=0){
                cerca=1;
                DiskDriver_readBlock(disk, ffb_open, db.file_blocks[pos]);
                fh->fcb=ffb_open;
            }
            next_block=db.header.next_block;
        }
        
        if(cerca)
            return fh;
        else{
                printf("file non trovato\n");
                free(fh);
        }
    }
    return NULL;
}


// closes a file handle (destroyes it)
int SimpleFS_close(FileHandle* f){
    if(f==NULL)
        return -1;
    free(f->fcb);
    free(f);
    return 0;
}

// writes in the file, at current position for size bytes stored in data
// overwriting and allocating new space if necessary
// returns the number of bytes written
int SimpleFS_write(FileHandle* f, void* data, int size){
    if(f==NULL || data==NULL || size<0)
        return -1;

    int written_bytes=0;
    int pos=f->pos_in_file;
    int bytes=size;
    int ret;
    FirstFileBlock* ffb=f->fcb;

    //controllo in ffb
    if(pos<FFB && bytes <= FFB-pos){
        memcpy(ffb->data + pos, (char*)data, bytes);
        written_bytes+=bytes;
        if(f->pos_in_file+written_bytes > ffb->fcb.size_in_bytes)
            ffb->fcb.size_in_bytes=f->pos_in_file + written_bytes;
        DiskDriver_aggiorna(f->sfs->disk, ffb, ffb->fcb.block_in_disk);
        return written_bytes;
    }
    else if(pos < FFB && bytes > FFB-pos){
        memcpy(ffb->data + pos, (char*)data, FFB - pos);
        written_bytes+=FFB - pos;
        bytes=size-written_bytes;
        DiskDriver_aggiorna(f->sfs->disk, ffb, ffb->fcb.block_in_disk);
        pos=0;
    }
    //memorizzo indici del blocco precedente, successivo e indice precedente
    int pre_block=ffb->fcb.block_in_disk;
    int next_block=ffb->header.next_block;
    int pre_index=ffb->header.block_in_file;
    FileBlock fb_aux;
    int one=0;
    if(next_block==-1)
        one=1;
    
    //in fb
    int fb_block=1;
    while(written_bytes < size){
        printf("uso blocco %d\n", fb_block++);
        //blocco da creare
        if(next_block==-1){
            FileBlock new={0};
            new.header.block_in_file=pre_index +1;
            new.header.next_block=-1;
            new.header.previous_block=pre_block;
            ffb->fcb.size_in_blocks +=1;
            //prendo il primo blocco libero
            next_block=DiskDriver_getFreeBlock(f->sfs->disk, pre_block);
            if(one==1){ //non ci sono altri blocchi allocati dopo
                ffb->header.next_block=next_block;
                DiskDriver_aggiorna(f->sfs->disk, ffb,ffb->fcb.block_in_disk);
                one=0;
                printf("nessun blocco successivo\n");
            }
            else{ //aggiorno blocco successivo
                fb_aux.header.next_block=next_block;
                DiskDriver_aggiorna(f->sfs->disk, &fb_aux,pre_block);
            }
            DiskDriver_writeBlock(f->sfs->disk, &new, next_block);
            fb_aux=new;
        }
        //se blocco esiste
        else{
            ret=DiskDriver_readBlock(f->sfs->disk,&fb_aux, next_block);
            if(ret==-1)
                return -1;
        }
        
        
        if(pos < FB && bytes <= FB-pos){
        memcpy(fb_aux.data + pos, (char*)data + written_bytes, bytes);
        written_bytes+=bytes;
            if(f->pos_in_file+written_bytes > ffb->fcb.size_in_bytes)
                ffb->fcb.size_in_bytes=f->pos_in_file + written_bytes;
            DiskDriver_aggiorna(f->sfs->disk, ffb, ffb->fcb.block_in_disk);
            DiskDriver_aggiorna(f->sfs->disk, &fb_aux, next_block);
            return written_bytes;
        }
        else if(pos < FB && bytes > FB-pos){
            memcpy(fb_aux.data + pos, (char*)data+written_bytes, FB-pos);
            written_bytes+= FB - pos;
            bytes=size-written_bytes;
            DiskDriver_aggiorna(f->sfs->disk, &fb_aux, next_block);
            pos=0;
        }
        pre_block=next_block;
        next_block=fb_aux.header.next_block;
        pre_index=fb_aux.header.block_in_file;
    }
    return written_bytes;
}

// writes in the file, at current position size bytes stored in data
// overwriting and allocating new space if necessary
// returns the number of bytes read
int SimpleFS_read(FileHandle* f, void* data, int size){
    if(f==NULL || data==NULL || size<0)
        return -1;
    FirstFileBlock* ffb=f->fcb;
    //FileBlock fb;
    int pos=f->pos_in_file; //posizione per il cursore
    int written_bytes=ffb->fcb.size_in_bytes;
    if(size+pos > written_bytes){
        memset(data, 0, size);
        return -1;
    }
    int bytes_read=0;
    int bytes=size;
    //in ffb
    if(pos<FFB && bytes <= FFB - pos){
        memcpy(data, ffb->data+ pos, bytes);
        bytes_read +=bytes;
        bytes=size- bytes_read;
        f->pos_in_file += bytes_read; //aggiorno il cursore
        return bytes_read;
    }
    //se devo leggere più del FFB
    else if(pos<FFB && bytes > FFB - pos){
        memcpy(data, ffb->data+ pos, FFB - pos);
        bytes_read +=FFB - pos;
        bytes=size - bytes_read;
        pos=0;
    }
    FileBlock fb_aux;
    //in fb
    if(bytes_read<size && ffb->header.next_block !=-1){
        DiskDriver_readBlock(f->sfs->disk, &fb_aux, ffb->header.next_block);
        
        if(pos<FB && bytes <= FB - pos){
            memcpy(data+bytes_read, fb_aux.data+ pos, bytes);
            bytes_read +=bytes;
            bytes=size- bytes_read;
            f->pos_in_file += bytes_read;
            return bytes_read;
        }
        else if(pos<FB && bytes > FB - pos){
            memcpy(data+bytes_read, fb_aux.data+ pos, FB - pos);
            bytes_read +=FB - pos;
            bytes=size - bytes_read;
            pos=0;
        }
    }

    return bytes_read;
}

// returns the number of bytes read (moving the current pointer to pos)
// returns pos on success
// -1 on error (file too short)
int SimpleFS_seek(FileHandle* f, int pos){
    if(pos<0)
        return -1;
    FirstFileBlock* ffb=f->fcb;
    if(pos> ffb->fcb.size_in_bytes){
        printf("errore out size\n");
        return -1;    
    }
    f->pos_in_file=pos;
    return pos;
}

//funzione aggiuntiva
//uguale a File_Dir solo che li uso FirstFileBlock e qui FirstDirectoryBlock
static int DirectoryExist(DiskDriver* disk, int space, int* file_blocks, const char* filename){
    FirstDirectoryBlock fdb;
    for(int i=0; i<space; i++){
        if(file_blocks[i]>0 && DiskDriver_readBlock(disk, &fdb, file_blocks[i])!=-1){
            if(!strncmp(fdb.fcb.name, filename,128)) //per limitare il confronto fra le due stringhe al piu' a n caratteri
                return i;
        }
    }
    return -1;
}

// seeks for a directory in d. If dirname is equal to ".." it goes one level up
// 0 on success, negative value on error
// it does side effect on the provided handle
int SimpleFS_changeDir(DirectoryHandle* d, char* dirname){
    if(d==NULL || dirname==NULL)
        return -1;
    int ret;
    //in caso di ".." torno alla cartella genitore
    if(!strncmp(dirname, "..",2)){
        if(d->dcb->fcb.block_in_disk==0) //se ci troviamo nella radice
            return -1;
        //else, ci troviamo in una cartella che non è la radice
        //passando alla cartella precedente e leggendo cartella genitore
        FirstDirectoryBlock* parent=malloc(sizeof(FirstDirectoryBlock));
        //leggo padre
        ret=DiskDriver_readBlock(d->sfs->disk, parent,d->directory->fcb.block_in_disk);
        d->dcb=d->directory;
        //d->current_block=&(d->dcb->header);
        d->pos_in_block=0;
        //d->directory=parent;
        int parent_block=d->dcb->fcb.directory_block;
        if(parent_block==-1){ //è la root
            d->directory==NULL;
            return 0;
        }
        if(ret==-1)
            d->directory=NULL;
        else    
            d->directory=parent; //salvo il padre
        
        return 0;
    }
    //else, se dirname diverso da ".."
    FirstDirectoryBlock* fdb=d->dcb;
    DiskDriver* disk=d->sfs->disk;
    FirstDirectoryBlock* child=malloc(sizeof(FirstDirectoryBlock));
        
    //controllo con DirectoryExist se si trova in FDB o DB
    int pos=DirectoryExist(disk, FDB, fdb->file_blocks, dirname);
    if(pos>=0){
        DiskDriver_readBlock(disk, child, fdb->file_blocks[pos]);
        d->pos_in_block=0;
        d->directory=fdb;
        d->dcb=child;
        return 0;
    }
    DirectoryBlock db;
    int next_block=fdb->header.next_block;
    while(next_block !=-1){
        ret=DiskDriver_readBlock(disk, &db, next_block);
        if(ret==-1)
            return -1;
        pos=DirectoryExist(disk, DB, db.file_blocks, dirname);
        if(pos>=0){
            //FirstDirectoryBlock* child=malloc(sizeof(FirstDirectoryBlock));
            DiskDriver_readBlock(disk, child, db.file_blocks[pos]);
            d->directory=fdb;
            d->dcb=child;
            return 0;
        }
        next_block=db.header.next_block;
    }
    return -1; 
}

// creates a new directory in the current one (stored in fs->current_directory_block)
// 0 on success
// -1 on error
int SimpleFS_mkDir(DirectoryHandle* d, char* dirname){
    if(d==NULL || dirname ==NULL)
        return -1; 
    /*
    //se non ci sono blocchi liberi, errore
    if(d->sfs->disk->header->free_blocks < 1)
        return -1;
    */
    FirstDirectoryBlock* fdb=d->dcb;
    DiskDriver* disk=d->sfs->disk;
    int ret;
    
    //controllo dove si trova il file, se in FDB o DB
    if(fdb->num_entries >0){
        //verifico se in FDB
        ret=File_Dir(disk, FDB, fdb->file_blocks, dirname);
        if(ret>=0){ //file esiste gia in fdb
            printf("Il file %s già esiste in FDB\n", dirname);
            return -1;
        }
        //se in DB
        DirectoryBlock db;
        int next_block=fdb->header.next_block;
        while(next_block!=-1){
            ret=DiskDriver_readBlock(disk, &db, next_block);
            if(ret<0)
                printf("ERRORE: readBlock()");
                return -1;
            ret=File_Dir(disk, DB, db.file_blocks, dirname);
            if(ret>=0) //file esiste già in db
                printf("Il file %s già esiste in DB\n", dirname);
                return -1;
            next_block=db.header.next_block;
        }
    }

    //dopo tutti controlli creo il file su blocco vuoto
    int block_free=DiskDriver_getFreeBlock(disk, disk->header->first_free_block);
    if(block_free==-1)
        printf("ERRORE: no blocchi vuoti\n");
        return -1;
    
    FirstDirectoryBlock* fdb_new=calloc(1, sizeof(FirstFileBlock)); 
    fdb_new->header.previous_block=-1;
    fdb_new->header.next_block=-1;
    fdb_new->header.block_in_file=0;
    fdb_new->fcb.directory_block=fdb->fcb.block_in_disk;
    fdb_new->fcb.block_in_disk=block_free;
    fdb_new->fcb.is_dir=1;
    strcpy(fdb_new->fcb.name, dirname);

    //scrivo file su disco
    ret=DiskDriver_writeBlock(disk, fdb_new, block_free);
    if(ret<0)
        return -1;
    
    int found=0;
    int db_create=0; //per segnalare se bisogno di creare nuova cartella
    int in_db=0; //0 se fdb, 1 se db
    int entry=0; 
    int block_dir=0; //blocco directory
    int block_disk=fdb->fcb.block_in_disk; //blocco disco
    int end_db=0; //per vedere se spazio esaurito o no
    int i=0;
    DirectoryBlock db;

    //cerco prima in fdb se posto libero
    if(fdb->num_entries<FDB){
        int* blocks=fdb->file_blocks; //o fdb_new
        for(i=0;i<FDB;i++){
            if(blocks[i]==0){ //vi sono blocchi liberi in fdb
                found=1;
                entry=i;
                printf("trovati[%d] in fdb", entry);
                break;
            }
        }
    }
    else{ //vado a cercare spazio nelle altre cartelle, in db
        in_db=1;
        int next_block=fdb->header.next_block; //o fdb_new
        while(next_block!=1 && !found){
            ret=DiskDriver_readBlock(disk, &db, next_block);
            if(ret==-1){
                printf("errore\n");
                DiskDriver_freeBlock(disk,fdb->fcb.block_in_disk);//o fdb_new
                return -1;
            }
            int* blocks=db.file_blocks;
            block_dir++;
            block_disk=next_block;
            for(i=0; i<DB; i++){
                if(blocks[i]==0){ //vi sono blocchi liberi in db
                    found=1;
                    entry=i;
                    printf("trovati[%d] in db", entry);
                    break;
                }
                end_db++;
            }
            db_create=1; //non bisogno di creare cartella
            next_block=db.header.next_block;
        }
    }
    //controllo se ho esaurito spazio in db
    if(end_db>DB-1){
        printf("spazio esaurito in db, %d\n", end_db);
    }
    //se non c'è spazio neanche nelle altre cartelle ne creo nuove
    if(!found){
        printf("creazione db\n");
        DirectoryBlock new={0};
        new.header.next_block=-1;
        new.header.block_in_file=block_dir;
        new.header.previous_block=block_disk;
        new.file_blocks[0]=block_free;

        //prendo blocco libero per allocare
        int new_db=DiskDriver_getFreeBlock(disk, disk->header->first_free_block);
        if(new_db==-1){
            printf("nessun blocco libero\n");
            DiskDriver_freeBlock(disk,fdb->fcb.block_in_disk);//o fdb_new
            return -1;
        }
        //scrivo su disco
        ret=DiskDriver_writeBlock(disk,&new, new_db);
        if(ret == -1){
			printf("ERRORE: writeBlock()\n");
			DiskDriver_freeBlock(disk, fdb->fcb.block_in_disk);
			return -1;
		}	
        //se è 0 ho bisogno di creare nuova cartella
        if(db_create==0)
            fdb_new->header.next_block=new_db;
        else  
            db.header.next_block=new_db;
        
        db=new;
        block_disk=new_db;
    }
    //aggiorno le varie cartelle in fdb e db
    if(in_db==0){ //ci troviamo in fdb
        printf("salvo %s in FDB\n",dirname);
        fdb->num_entries++; //o fdb_new
        fdb->file_blocks[entry]=block_free; //o fdb_new
        DiskDriver_aggiorna(disk,fdb,fdb->fcb.block_in_disk); //o fdb_new
    }
    else{ //in db
        printf("salvo %s in DB\n",dirname);
        fdb_new->num_entries++; //o fdb_new
        DiskDriver_aggiorna(disk,fdb,fdb->fcb.block_in_disk);
        fdb->file_blocks[entry]=block_free;
        DiskDriver_aggiorna(disk,&db,block_disk);
    }
    /*
    //tutte info su disco e restituisco FileHandle
    FileHandle* fh=malloc(sizeof(FileHandle));
    fh->sfs=d->sfs;
    fh->fcb=ffb;
    fh->directory=fdb;
    fh->pos_in_file=0;
    return fh;
    */
    return 0;
}

// removes the file in the current directory
// returns -1 on failure 0 on success
// if a directory, it removes recursively all contained files
int SimpleFS_remove(DirectoryHandle* d, char* filename){
    if(d==NULL || filename==NULL)
        return -1;
    FirstDirectoryBlock* fdb=d->dcb;
    int posFile=File_Dir(d->sfs->disk,FDB, fdb->file_blocks,filename);
    int fdbFile=1;
    int ret;
    
    DirectoryBlock* db=(DirectoryBlock*)malloc(sizeof(DirectoryBlock));
    int next_block=fdb->header.next_block;
    int block=fdb->fcb.block_in_disk;

    while(posFile==-1){
        if(next_block !=-1){
            fdbFile=0;
            ret=DiskDriver_readBlock(d->sfs->disk, db, next_block);
            if(ret == -1){
				printf("ERRORE: readBlock()\n");
				return -1;
			}
            posFile=File_Dir(d->sfs->disk, DB, db->file_blocks, filename);
            block=next_block;
            next_block=db->header.next_block;
        }
        else //blocchi finiti 
            return -1;
    }
    int check;
    if(fdbFile==0)
        check=db->file_blocks[posFile];
    else   
        check=fdb->file_blocks[posFile];
    
    FirstFileBlock ffb_rm;
    ret=DiskDriver_readBlock(d->sfs->disk, &ffb_rm, check);
    if(ret == -1){
		printf("ERRORE: readBlock\n");
		return -1;
	}
    if(ffb_rm.fcb.is_dir==0){ //elimino file
        FileBlock fb_tmp;
        int next_block=ffb_rm.header.next_block;
        int block=check;
        while(next_block!=-1){
            ret=DiskDriver_readBlock(d->sfs->disk, &fb_tmp, next_block);
            if(ret == -1){
                printf("ERRORE: readBlock\n");
                return -1;
            }
            block=next_block;
            next_block=fb_tmp.header.next_block;
            DiskDriver_freeBlock(d->sfs->disk, block);
        }
        DiskDriver_freeBlock(d->sfs->disk, check);
        d->dcb=fdb;
        ret=0;
    }
    else{ //elimino dir
        FirstDirectoryBlock fdb_rm;
        ret=DiskDriver_readBlock(d->sfs->disk, &fdb_rm, check);
        if(ret == -1){
            printf("ERRORE: readBlock\n");
            return -1;
        }
        if(fdb_rm.num_entries>0){
            ret=SimpleFS_changeDir(d,fdb_rm.fcb.name);
            if(ret == -1){
                printf("ERRORE: changeDir\n");
                return -1;
            }
            int i;
            //eliminazione FDB di tutti i file nella dir
            for(i=0; i<FDB; i++){
                FirstFileBlock ffb;
                ret=DiskDriver_readBlock(d->sfs->disk, &ffb, fdb_rm.file_blocks[i]);
                if(fdb_rm.file_blocks[i]>0 && ret!=-1)
                    SimpleFS_remove(d, ffb.fcb.name);
            }
            //eliminazione DB di tutti i file nella dir
            int next_block=fdb_rm.header.next_block;
            int block=check;
            DirectoryBlock db;
            while(next_block!=-1){
                ret=DiskDriver_readBlock(d->sfs->disk, &db, next_block);
                if(ret == -1){
                    printf("ERRORE: readBlock\n");
                    return -1;
                }
                int j;
                for(j=0; j<DB; j++){
                    FirstFileBlock ffb;
                    ret=DiskDriver_readBlock(d->sfs->disk, &ffb, db.file_blocks[j]);
                    if(ret == -1){
                        printf("ERRORE: readBlock\n");
                        return -1;
                    }
                    //if(fdb->file_blocks[j]>0 && ret!=-1)
                        SimpleFS_remove(d, ffb.fcb.name);
                }
                block=next_block;
                next_block=db.header.next_block;
                DiskDriver_freeBlock(d->sfs->disk, block);
            }
            DiskDriver_freeBlock(d->sfs->disk, check);
            d->dcb = fdb;
            ret=0;
        }
        else{
            DiskDriver_freeBlock(d->sfs->disk, check);
            d->dcb = fdb;
            ret=0;
        }
    }
    if(fdbFile){
        fdb->file_blocks[posFile]=-1;
        fdb->num_entries-= 1;
        DiskDriver_aggiorna(d->sfs->disk,fdb, fdb->fcb.block_in_disk);
        free(db);
        return ret;
    }
    else{
        db->file_blocks[posFile]=-1;
        fdb->num_entries-= -1;
        DiskDriver_aggiorna(d->sfs->disk,db,block);
        DiskDriver_aggiorna(d->sfs->disk,fdb, fdb->fcb.block_in_disk);
        free(db);
        return ret;
    }
    return -1;
}
