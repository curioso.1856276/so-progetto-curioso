#include "bitmap.h"
#include <stdio.h>
#define bin 0x01

//implementazioni funzioni di bitmap.h

//utilizzato per convertire un indice di blocco in un indice nell'array
//carattere indica offset all'interno di array
BitMapEntryKey BitMap_blockToIndex(int num){
    BitMapEntryKey map;
    int byte=num/8;
    map.entry_num=byte;
    char offset=num-(byte*8);
    map.bit_num=offset;
    return map;
}

//conversione del bit in un intero
int BitMap_indexToBlock(int entry, uint8_t bit_num){
    if(entry<0 || bit_num<0)
        return -1;
    return (entry*8)+ bit_num;
}

//aggiunta funzione per facilitare la posizione del bit
int BitMap_getBit(BitMap* bmap, int p){
    if(p>=bmap->num_bits)
        return -1;
    BitMapEntryKey map= BitMap_blockToIndex(p);
    return bmap->entries[map.entry_num]>>map.bit_num & bin;
}
//restituisce indice (start) del primo bit
int BitMap_get(BitMap* bmap, int start, int status){
    if(start>bmap->num_bits)
        return -1;
    while(start<bmap->num_bits){
        if(BitMap_getBit(bmap, start)==status)
            return start;
        start++;
    }
    return -1;
}

int BitMap_set(BitMap* bmap, int pos, int status){
    if(pos>= bmap->num_bits)
        return -1;
    BitMapEntryKey map=BitMap_blockToIndex(pos);
    unsigned char num_bit=1<< map.bit_num; 
    //num_bit << map.bit_num; //capire se è corretto
    unsigned char entrys= bmap->entries[map.entry_num];
    if(status==1){
        //OR logico
        bmap->entries[map.entry_num]=entrys | num_bit;
        return entrys | num_bit;
    }
    else{
        //AND logico 
        bmap->entries[map.entry_num]=entrys & (~num_bit);
        return entrys & (~num_bit);
    }
}

//aggiunta di funzione per stampare Bitmap
void BitMap_print(BitMap* bmap, int a){
    int b;
    char mask[]={128,64,32,16,8,4,2,1};
    for(int i=0;i<8; i++){
        b=((bmap->entries[a] & mask[i]) !=0);
        printf("%d",b);
    }
}