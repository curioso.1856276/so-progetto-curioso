#include <fcntl.h>   //per open,posix_..
#include <sys/stat.h> //open
#include <unistd.h>
#include <sys/mman.h> 
#include <stdio.h>
#include "disk_driver.h"

void DiskDriver_init(DiskDriver* disk, const char* filename, int num_blocks){
	if(disk == NULL || filename == NULL || num_blocks < 1){
		printf("ERRORE: parametri non validi\n");
		return;
	}
	int bsize = num_blocks / 8;	// Dimensione bitmap
	if(num_blocks % 8) ++bsize;	// Arrotondo
	
	int fd;	// File descriptor da restituire
	// Controllo se posso accedere al filename e accedo / creo
	int acc = access(filename, F_OK) == 0; 
	
	if(acc){

		fd = open(filename, O_RDWR, (mode_t)0666);
		if(!fd){
			printf("Errore nell'apertura del file\n");
			return;
		}
	}
	else{
		fd = open(filename, O_CREAT | O_TRUNC | O_RDWR, (mode_t)0666);
		if(!fd){
			printf("Errore nella creazione del file\n");
			return;
		}
		
		// Alloco il nuovo disco
		int ret;
		//eventuale archiviazione richiesta tra 0 e size... si alloca sul supporto di allocazione
		ret = posix_fallocate(fd, 0, sizeof(DiskHeader) + bsize);
		if(ret){
			printf("Errore nell'allocazione del disco");
			return;
		}
	}

	// Devo mappare il disco in memoria da restituire
	DiskHeader* map = (DiskHeader*) mmap(0,sizeof(DiskHeader)+bsize,PROT_READ | PROT_WRITE,MAP_SHARED,fd,0); 
	if(map == MAP_FAILED){
		close(fd);
		printf("ERROR: mmap()\n");
		return;
	}
	
	// Lo setto 
	if(!acc){
		map->num_blocks = num_blocks;
		map->bitmap_blocks = num_blocks;
		map->bitmap_entries = bsize;
		
		map->free_blocks = num_blocks;
		map->first_free_block = 0;
	}

	// Lo salvo
	disk->header = map;
	disk->bitmap_data = (char*)map + sizeof(DiskHeader);
	disk->fd = fd;
}


int DiskDriver_readBlock(DiskDriver* disk, void* dest, int block_num){
	if(block_num > disk->header->bitmap_blocks || block_num < 0 || dest == NULL || disk == NULL){
		printf("ERRORE: parametri non validi\n");
		return -1;
	}

	// Uso la bitmap per controllare se il blocco è libero
	BitMap bmap;
	bmap.num_bits = disk->header->bitmap_blocks;
	bmap.entries = disk->bitmap_data;

	// Se il blocco è vuoto -> non leggo
	if(!BitMap_getBit(&bmap, block_num)) return -1;
	
	// Lo leggo
	int ret = pread(disk->fd,dest,BLOCK_SIZE,sizeof(DiskHeader) + disk->header->bitmap_entries + (block_num*BLOCK_SIZE));
	if(ret < 0){
		printf("ERRORE: pread()\n");
		return -1;
	}

	return 0;
}


int DiskDriver_writeBlock(DiskDriver* disk, void* src, int block_num){
	if(block_num > disk->header->bitmap_blocks || block_num < 0 || src == NULL || disk == NULL){
		printf("ERRORE: parametri non validi\n");
		return -1;
	}

	BitMap bmap;
	bmap.num_bits = disk->header->bitmap_blocks;
	bmap.entries = disk->bitmap_data;

	// Se il blocco è pieno -> non scrivo
	if(BitMap_getBit(&bmap, block_num)) return -1;
	
	// Se sto scrivendo sull'attuale primo blocco libero -> aggiorno il primo blocco libero
	if(block_num == disk->header->first_free_block)
		disk->header->first_free_block = DiskDriver_getFreeBlock(disk, block_num + 1);
	
	// Setto che il blocco è quindi ora occupato
	if(BitMap_set(&bmap, block_num, 1) == -1) return -1;

	// Aggiorno il numero dei blocchi liberi
	disk->header->free_blocks -= 1;
	
	// Lo scrivo
	int ret = pwrite(disk->fd,src,BLOCK_SIZE,sizeof(DiskHeader) + disk->header->bitmap_entries + (block_num*BLOCK_SIZE));
	if(ret < 0){
		printf("ERRORE: pwrite()\n");
		return -1;
	}

	return 0;
}

int DiskDriver_getFreeBlock(DiskDriver* disk, int start){
	if(start > disk->header->bitmap_blocks){
		printf("ERRORE: parametri non validi\n");
		return -1;
	}
	
	BitMap bmap;
	bmap.num_bits = disk->header->bitmap_blocks;
	bmap.entries = disk->bitmap_data;
	
	// Prendo e resttuisco il primo blocco libero
	return BitMap_get(&bmap, start, 0);
}

int DiskDriver_freeBlock(DiskDriver* disk, int block_num){
	if(block_num > disk->header->bitmap_blocks || block_num < 0 || disk == NULL){
		printf("ERRORE: parametri non validi\n");
		return -1;
	}
	
	BitMap bmap;
	bmap.num_bits = disk->header->bitmap_blocks;
	bmap.entries = disk->bitmap_data;
	
	// Verifico che il bit non sia già libero
	if(!BitMap_getBit(&bmap, block_num)) return -1;
	
	// Allora lo libero io -> lo setto a 0
	if(BitMap_set(&bmap, block_num, 0) < 0){
		printf("ERRORE: BitMap_set() - freeBlock\n");
		return -1;
	}
	
	// In caso devo aggiornare il primo blocco libero
	if(block_num < disk->header->first_free_block)
		disk->header->first_free_block = block_num;
	
	// Aggiorno il numero dei blocchi liberi
	disk->header->free_blocks++;
	
	return 0;
}

// msync: scarica le modifiche apportate alla copia interna di un file che
//	è stato mappato in memoria
int DiskDriver_flush(DiskDriver* disk){
	int bsize = disk->header->num_blocks / 8;	// Dimensione bitmap
	if(disk->header->num_blocks % 8) bsize++;	// Arrotondo
	//msync scrive tutte le copie modificate delle pagine nell'intervallo scelto
	//MS_SYNC usato per eseguire le scritture sincrone 
	int ret = msync(disk->header,(size_t)sizeof(DiskHeader) + bsize, MS_SYNC);
	if(ret < 0){
		printf("ERROR: msync()");
		return -1;
	}
	return 0;
}

// Come la write, ma devo aggiornare un blocco -> non mi serve la bitmap
// 	la write infatti non mi avrebbe scritto su un blocco già occupato
int DiskDriver_aggiorna(DiskDriver* disk, void* src, int block_num){
	if(block_num > disk->header->bitmap_blocks || block_num < 0 || src == NULL || disk == NULL){
		printf("Errori non validi");
		return -1;
	}
	int fd = disk->fd;
	int ret = pwrite(fd, src, BLOCK_SIZE, sizeof(DiskHeader)+disk->header->bitmap_entries+(block_num*BLOCK_SIZE));
	if(ret < 0){
		printf("ERRORE: pwrite() - aggiorna");
		return -1;
	}
	return 0;
}
